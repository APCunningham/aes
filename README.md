# USAGE #

## Requirements ##
Maven 3  
Java 1.7

## Build ##
mvn package

## Run ##
java -jar Application.jar [OPTIONS] [< INPUT.TXT][> OUTPUT.TXT]

E.G:  
<Encryption>
java -jar Application.jar < plain.txt > cipher.txt

<Decryption>
java -jar Appication.jar -d < cipher.txt > plain.txt

## Options ##
* The input can be:  
*  16 characters (alphanumeric string length 16)  
*  32 digit hexademical (hex string length 32)  
*  128 digit binary (binary string length 128)  
*  
* Command line inputs:  
*  -i=[filename]   input file  
*  -o=[filename]   output file  
*  -d              decrypt  
*  -hex            hexadecimal output  
*  -b64            base64 output  
*  -mat            matrix output  
*  -n              normal operation (non assignment based output)  
*  -fips           FIPS197 style output

## Authors ##
Andrew Cunningham    c3011901  
Jake Spencer-Goodsir c3147019