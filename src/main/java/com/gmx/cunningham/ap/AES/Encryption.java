package com.gmx.cunningham.ap.AES;

import static com.gmx.cunningham.ap.AES.AESUtils.*;

/**
 * Encryption for 128-bit AES
 * @author Andrew Cunningham
 * @author Jake Spencer-Goodsir
 * @since 05/05/2015
 */
public class Encryption extends AES{

    private int skip;

    /**
     * Constructor
     * Creates new encryption with key and plaintext
     * @param keyS 128-bit key
     * @param plainS 128-bit plaintext
     */
    public Encryption(String keyS, String plainS){
        this(keyS,plainS,0);
    }

    /**
     * Constructor
     * Creates new encryption with key and plaintext
     * @param keys full key schedule
     * @param plainS 128-bit plaintext
     */
    public Encryption(Block[] keys, String plainS){
        this(keys,plainS,0);
    }

    /**
     * Constructor
     * Creates new encryption with key and plaintext. Skips a step each round:
     * 0 - no skip
     * 1 - sub bytes
     * 2 - shift rows
     * 3 - mix columns
     * 4 - add round key
     * @param keyS 128-bit key
     * @param plainS 128-bit plaintext
     * @param skip index of skipped step
     */
    public Encryption(String keyS, String plainS, int skip){
        this(generateRoundKeys(new Block(keyS)), new Block(plainS), skip);
    }

    /**
     * Constructor
     * Creates new encryption with key and plaintext. Skips a step each round:
     * 0 - no skip
     * 1 - sub bytes
     * 2 - shift rows
     * 3 - mix columns
     * 4 - add round key
     * @param keys full key schedule
     * @param plainS 128-bit plaintext
     * @param skip index of skipped step
     */
    public Encryption(Block[] keys, String plainS, int skip){
        this(keys, new Block(plainS), skip);
    }

    /**
     * Constructor
     * Creates new encryption with key and plaintext. Skips a step each round:
     * 0 - no skip
     * 1 - sub bytes
     * 2 - shift rows
     * 3 - mix columns
     * 4 - add round key
     * @param keys full key schedule
     * @param plain 128-bit plaintext block
     * @param skip index of skipped step
     */
    public Encryption(Block[] keys, final Block plain, int skip){
        setRound(0);
        setKeys(keys);
        setPlain(plain);
        setCipher(new Block(plain.matrix()));
        this.skip = skip;
    }

    /**
     * checks if there is another round to run
     * @return true if there is another round to run
     */
    @Override
    public boolean hasNext(){
        return getRound()<11;
    }

    /**
     * runs next round of encryption
     */
    @Override
    public void nextRound(){
        if(getRound() == 0) {
            //fips output "input" tag first round only
            append(String.format("round[%2d].iinput: %s\n", getRound(), getCipher()));
        } else {
            //fips output "start" tag all rounds except first
            append(String.format("round[%2d].istart: %s\n", getRound(), getCipher()));
        }

        if(getRound() <= 10 && getRound() >= 0){

            // does not run in round 0 or when AES1 (skip is 1)
            if(getRound()!=0&&skip!=1) {
                setCipher(subBytes(getCipher()));

                //fips output
                append(String.format("round[%2d].is_box: %s\n", getRound(), getCipher()));
            }

            // does not run in getRound() 2 or when AES2 (skip is 2)
            if(getRound()!=0&&skip!=2) {
                setCipher(shiftRows(getCipher()));

                //fips output
                append(String.format("round[%2d].is_row: %s\n", getRound(), getCipher()));
            }

            // does not run in rounds 10 and 0 or when AES3 (skip is 3)
            if(getRound()!=10&&getRound()!=0&&skip!=3) {
                setCipher(mixColumns(getCipher()));

                //fips output
                append(String.format("round[%2d].im_col: %s\n", getRound(), getCipher()));
            }

            // Adds round key to fips style output
            append(String.format("round[%2d].ik_sch: %s\n", getRound(), getRoundKey()));

            //Does not run if AES4 (skip is 4)
            if(skip!=4) setCipher(addRoundKey(getRoundKey(), getCipher()));

            //Adds output to fips style output
            if(getRound() == 10) append(String.format("round[%2d].ioutout: %s\n", getRound(), getCipher()));
        } else return;
        roundpp();
    }

    /**
     * Encrypts plaintext with key
     * @param key 128-bit key
     * @param plaintxt 128-bit plain text
     * @return encrypted block
     */
    public static Block encrypt(String key, String plaintxt){
        Encryption enc = new Encryption(key, plaintxt);
        while(enc.hasNext()) enc.nextRound();
        return enc.getCipher();
    }
}
