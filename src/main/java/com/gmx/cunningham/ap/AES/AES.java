package com.gmx.cunningham.ap.AES;

/**
 * @author Andrew Cunningham
 * @author Jake Spencer-Goodsir
 * @since 08/05/2015
 */
public abstract class AES {
    //keys for the encryption/decryption
    private Block[] keys;

    //current plain text will be input for encryption and running output for decryption
    private Block plain;

    //current cipher text will be input for decryption and running output for encryption
    private Block cipher;

    //current round
    private int round;

    //output for the fips style output
    private StringBuilder output = new StringBuilder();

    /**
     * Runs next round of the encryption/decryption
     */
    public abstract void nextRound();

    /**
     * Checks if there is another round to execute
     * @return true if there is another round
     */
    public abstract boolean hasNext();

    /**
     * Original key for the encryption/decryption
     * @return original key
     */
    public Block getKey() {
        return keys[0];
    }

    /**
     * Returns the current/original plaintext for decryption/encryption
     * @return current/original plaintext
     */
    public Block getPlain() {
        return plain;
    }

    /**
     * Returns the current/original cipher for encryption/decryption
     * @return current/original cipher text
     */
    public Block getCipher() {
        return cipher;
    }

    /**
     * Returns current rounds key
     * @return current round key
     */
    public Block getRoundKey() {
        return keys[round];
    }

    /**
     * Returns the current round
     * @return current round
     */
    public int getRound() {
        return round;
    }

    /**
     * Returns the fips style output
     * @return fips style output
     */
    public String getOutput(){
        return output.toString();
    }

    /**
     * sets the cipher block to a new block
     * @param c new cipher block
     */
    protected void setCipher(Block c){
        cipher = c;
    }

    /**
     * sets the current round
     * @param round round number
     */
    protected void setRound(int round){
        this.round = round;
    }

    /**
     * sets the keys for the encryption/decryption
     * @param keys generated keys
     */
    protected void setKeys(Block[] keys){
        this.keys = keys;
    }

    /**
     * Set current plain text
     * @param plain plain text
     */
    protected void setPlain(Block plain){
        this.plain = plain;
    }

    /**
     * round++
     */
    protected void roundpp(){
        round++;
    }

    /**
     * round--
     */
    protected void roundmm(){
        round--;
    }

    /**
     * Appends string to the fips style output
     * @param s output
     */
    protected void append(String s){
        output.append(s);
    }

    /**
     * Runs the entire encryption/decryption and return this AES
     * @return this AES
     */
    public AES run(){
        while(hasNext()){
            nextRound();
        }
        return this;
    }
}
