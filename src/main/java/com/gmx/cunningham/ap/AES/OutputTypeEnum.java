package com.gmx.cunningham.ap.AES;

/**
 * Types of output strings for AES 128-bit
 * @author Andrew Cunningham
 * @author Jake Spencer-Goodsir
 * @since 6/05/15
 */
public enum OutputTypeEnum {
    hex, bin, mat, b64;

    public static OutputTypeEnum value(String act){
        try{
            return OutputTypeEnum.valueOf(act);
        } catch (IllegalArgumentException ex){
            return OutputTypeEnum.bin;
        }
    }
}
