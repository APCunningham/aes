package com.gmx.cunningham.ap.AES;

/**
 * This Application encrypts and decrypts using an AES implementation.
 * It is used from the command line and only encrypts/decrypts one 128-bit block.
 *
 * The input can be:
 *  16 characters (alphanumeric string length 16)
 *  32 digit hexademical (hex string length 32)
 *  128 digit binary (binary string length 128)
 *
 * Command line inputs:
 *  -i=[filename]   input file
 *  -o=[filename]   output file
 *  -d              decrypt
 *  -hex            hexadecimal output
 *  -b64            base64 output
 *  -mat            matrix output
 *  -n              normal operation (non assignment based output)
 *  -fips           FIPS197 style output
 *
 * @author Andrew Cunningham
 * @author Jake Spencer-Goodsir
 * @since 03/05/15
 */
public class Application {
    private static Options OP = new Options();

    /**
     * Main application.
     * Sets the Options object from command line arguments and runs encryption
     * or decryption based on option selected.
     * @param args command line arguments
     */
    public static void main(String[] args) {
        OP.setup(args);

        String text = OP.scan().nextLine();
        String key = "00112233445566778899aabbccddeeff";
        if(text.equals("")){
            text = "000102030405060708090a0b0c0d0e0f";
        } else {
            key = OP.scan().nextLine();
        }

        if(OP.isEncrypt()) {
            if(OP.isNormal()) encryptNorm(key, text);
            else encrypt(key, text);
        } else decrypt(key, text);

        OP.close();
    }

    // Encrypts a string with a key as per the instructions of the assignment.
    // Outputs the data require to complete assignment.
    private static void encrypt(String key, String text){
        long start = System.currentTimeMillis();
        int[][] pvp1uk = new int[11][5];
        int[][] pukvk1 = new int[11][5];

        //creates a new list of keys for use in encryption
        //this is done to increase speed as the key list
        //does not change but k_1 does change and needs to
        //be recalculated each time.
        Block[] keys = AESUtils.generateRoundKeys(new Block(key));

        //Encrypts the P under K which is used for comparison
        Block[][] p = getPlainRounds(keys, text);

        //Runs through all changed bit plaintext and keys
        for(int i=0;i<128;i++) {
            AES[] p_1 = encryptChangeTextBit(keys, text, i);
            AES[] k_1 = encryptChangeKeyBit(key, text, i);
            pvp1uk = run(p, p_1, pvp1uk); //runs the entire encryption with round outputs
            pukvk1 = run(p, k_1, pukvk1); //runs the entire encryption with round outputs
        }
        long end = System.currentTimeMillis();

        //Output as per the assignment
        OP.out().print(header("ENCRYPTION", new Block(text), keys[0], p[10][0], end - start));
        OP.out().println(formatOutput("P and P_1 under K", pvp1uk));
        OP.out().println(formatOutput("P under K and K_1", pukvk1));
    }

    //Runs an encryption and returns the cipher blocks at end of each round used in comparisons
    private static Block[][] getPlainRounds(final Block[] keys, String text){
        Block[][] b = new Block[11][5];

        AES[] a = encryptions(keys, text);

        while(a[0].hasNext()){
            for(int j=0;j<a.length;j++) {
                a[j].nextRound();
                b[a[0].getRound()-1][j] = a[j].getCipher();
            }
        }

        return b;
    }

    // regular way of using encryption
    private static void encryptNorm(String key, String text){
        long st = System.currentTimeMillis();
        AES aes = AESUtils.getEncryption(key, text);
        st = System.currentTimeMillis()-st;
        OP.out().println(header("ENCRYPTION", aes.getPlain(), aes.getKey(), aes.getCipher(), st));

        //if using fips style output
        if(OP.isFips()) OP.out().println(aes.getOutput());
    }

    // decrypts the cipher text with given key
    private static void decrypt(String key, String cipher){
        long st = System.currentTimeMillis();
        AES aes = AESUtils.getDecryption(key, cipher);
        st = System.currentTimeMillis()-st;

        System.out.println(header("DECRYPTION", aes.getPlain(), aes.getKey(), aes.getCipher(), st));

        //if using fips style output
        if(OP.isFips()) OP.out().println(aes.getOutput());
    }

    // Changes a bit in the key and setups encryption
    private static AES[] encryptChangeKeyBit(String key, String plain, int bit){
        //calculate the row of the block matrix in which the bit will be changed
        int r = bit/32;

        //calculate the column of the block matrix in which the bit will be changed
        int c = bit%32/8;

        //create the new key
        Block k1 = new Block(key);

        //xor the current byte at row column with mod of the bit.
        //  M_rc xor X
        //where M = key's block matrix
        //      r = row           = {0, 1, 2, 3}
        //      c = column        = {0, 1, 2, 3}
        //      X = bit to change = {0, 1, ... , 7}
        k1.set(r, c, k1.get(r, c) ^ (int) Math.pow(2, bit % 8));

        return encryptions(AESUtils.generateRoundKeys(k1), plain);
    }

    // Changes a bit in the plaintext and setups encryption
    private static AES[] encryptChangeTextBit(Block[] keys, String plain, int bit){
        //calculate the row of the block matrix in which the bit will be changed
        int r = bit/32;

        //calculate the column of the block matrix in which the bit will be changed
        int c = bit%32/8;

        //create new plaintext
        Block p1 = new Block(plain);

        //xor the current byte at row column with mod of the bit.
        //  M_rc xor X
        //where M = key's block matrix
        //      r = row           = {0, 1, 2, 3}
        //      c = column        = {0, 1, 2, 3}
        //      X = bit to change = {0, 1, ... , 7}
        p1.set(r, c, p1.get(r, c) ^ (int) Math.pow(2,bit%8));

        return encryptions(keys, p1.toHexString());
    }

    //Create 5 AES encryptions for the assignment
    private static AES[] encryptions(final Block[] keys, String plainString){
        AES[] enc = new Encryption[5];
        for(int i=0;i<enc.length;i++) enc[i] = new Encryption(keys,plainString,i);
        return enc;
    }

    //Run an iteration of the encryption and collect stats at end of each round
    private static int[][] run(Block[][] e, AES[] e1, int[][] data){
        while(e1[0].hasNext()){
            for(AES enc: e1) enc.nextRound();
            for(int i=0;i<data[0].length;i++)
                data[e1[0].getRound()-1][i] += Block.difference(e[e1[0].getRound()-1][i],e1[i].getCipher());
        }
        return data;
    }

    //Returns a string with the header of the file
    private static String header(String type, Block p, Block k, Block c, long runtime){
        return type + "\n"
                + "Plaintext P: " + p.to(OP.getOtype()) + "\n"
                + "Key K: " + k.to(OP.getOtype()) + "\n"
                + "Ciphertext C: " + c.to(OP.getOtype()) + "\n"
                + "Running Time: " + runtime + "ms\n"
                + "Avalanche:\n";
    }

    //Returns headings for the output
    private static String headings(String title){
        return title+String.format("\n%7s%6s%6s%6s%6s%6s\n", "Round", "AES0", "AES1", "AES2", "AES3", "AES4");
    }

    //Formats the stats from the rounds
    private static String formatOutput(String title, int[][] data){
        StringBuilder sb = new StringBuilder();
        sb.append(headings(title));
        for(int i=0;i<data.length;i++){
            sb.append(String.format("%7d%6.0f%6.0f%6.0f%6.0f%6.0f\n", i,
                    data[i][0]/128.0,
                    data[i][1]/128.0,
                    data[i][2]/128.0,
                    data[i][3]/128.0,
                    data[i][4]/128.0
            ));
        }
        return sb.toString();
    }
}
