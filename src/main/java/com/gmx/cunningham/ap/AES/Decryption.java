package com.gmx.cunningham.ap.AES;

import static com.gmx.cunningham.ap.AES.AESUtils.*;

/**
 * Decryption for 128-bit AES
 * @author Andrew Cunningham
 * @author Jake Spencer-Goodsir
 * @since 5/05/15
 */
public class Decryption extends AES{
    /**
     * Constructor
     * Creates a new decryption for AES 128-bit
     * @param keyS 128-bit key string
     * @param cipherS 128-bit cipher text string
     */
    public Decryption(String keyS, String cipherS){
        this(generateRoundKeys(new Block(keyS)), cipherS);
    }

    /**
     * Constructor
     * Creates a new decryption for AES 128-bit
     * @param keys full key schedule for decryption
     * @param cipherS 128-bit cipher text string
     */
    public Decryption(Block[] keys, String cipherS){
        setRound(10);
        setKeys(keys);
        setCipher(new Block(cipherS));
        setPlain(new Block(cipherS));
    }

    /**
     * check if there is another round to run
     * @return true if another round still to run
     */
    @Override
    public boolean hasNext(){
        return getRound()>=0;
    }

    /**
     * runs next round of decryption
     */
    @Override
    public void nextRound() {

        if (getRound() == 10) {
            //adds input tag on first round for fips style output.
            append(String.format("round[%2d].iinput: %s\n", 10 - getRound(), getPlain()));
        } else {
            //adds start tag on all rounds except first for fips style output.
            append(String.format("round[%2d].istart: %s\n", 10 - getRound(), getPlain()));
        }

        if(getRound() < 11 && getRound() >= 0){
            if(getRound()!=10){
                //shift rows inverse run every round expect first
                setPlain(shiftRowsInverse(getPlain()));
                append(String.format("round[%2d].is_row: %s\n", 10 - getRound(), getPlain()));

                //sub byte inverse run every round except first
                setPlain(subBytesInverse(getPlain()));
                append(String.format("round[%2d].is_box: %s\n", 10 - getRound(), getPlain()));
            }

            //add sch tag to all rounds of decryption fips style output
            append(String.format("round[%2d].ik_sch: %s\n", 10 - getRound(), getRoundKey()));

            //add round key every round
            setPlain(addRoundKey(getRoundKey(), getPlain()));

            if(getRound()!=0&&getRound()!=10) {
                //add add tag to all rounds but first and last fips sytle output
                append(String.format("round[%2d].ik_add: %s\n", 10 - getRound(), getPlain()));

                //mix columns inverse all rounds except first and last
                setPlain(mixColumnsInverse(getPlain()));
            }

            //fips style output output tag for last round
            if(getRound()==0) append(String.format("round[%2d].ioutout: %s\n", 10-getRound(), getPlain()));
        } else return;
        roundmm();
    }

    /**
     * Decrypts cipher text with given key
     * @param key 128-bit string key for decryption
     * @param cipherTxt 128-bit cipher text for decryption
     * @return decrypted block
     */
    public static Block decrypt(String key, String cipherTxt){
        Decryption dec = new Decryption(key, cipherTxt);
        while(dec.hasNext()) dec.nextRound();
        return dec.getPlain();
    }
}
