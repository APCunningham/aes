package com.gmx.cunningham.ap.AES;

import java.io.*;
import java.util.Scanner;

/**
 * Command line options for AES application
 * @author Andrew Cunningham
 * @author Jake Spencer-Goodsir
 * @since 10/05/2015
 */
public class Options {
    //available options
    private enum OptionEnum {
        fips, n, b64, hex, mat, d, o, i, none;

        public static OptionEnum value(String act){
            //takes string and finds equiv option
            try{
                if(!act.startsWith("-")) return OptionEnum.none;
                return OptionEnum.valueOf(act.split("=")[0].replace("-", ""));
            } catch (IllegalArgumentException ex){
                return OptionEnum.none;
            }
        }
    }

    //main output style
    private OutputTypeEnum otype = OutputTypeEnum.bin;

    //true if encrypting false if decrypting
    private boolean encrypt = true;

    //true if not running assignment style encryption
    private boolean normal = false;

    //true if wanted fips197 style output for confirmation of correct encryption/decryption
    private boolean fips = false;

    //print stream to write output to
    private PrintStream out = System.out;

    //input stream tp get input from
    private InputStream in = System.in;

    //scanner to get input
    private Scanner sc = null;

    /**
     * Takes command line args and turn them into options
     * @param args command line args
     */
    public void setup(String[] args){
        for (String arg : args) {
            OptionEnum o = OptionEnum.value(arg);
            switch (o) {
                case hex:
                    otype = OutputTypeEnum.hex;
                    break;
                case b64:
                    otype = OutputTypeEnum.b64;
                    break;
                case mat:
                    otype = OutputTypeEnum.mat;
                    break;
                case d:
                    encrypt = false;
                    break;
                case n:
                    normal = true;
                    break;
                case fips:
                    //fips only works in normal mode
                    normal = true;
                    fips = true;
                    break;
                case i:
                    try {
                        in = new FileInputStream(arg.split("=")[1]);
                    } catch (FileNotFoundException e) {
                        System.err.println(e.getMessage());
                        System.exit(1);
                    }
                    break;
                case o:
                    try {
                        out = new PrintStream(arg.split("=")[1]);
                    } catch (FileNotFoundException e) {
                        System.err.println(e.getMessage());
                        System.exit(2);
                    }
                    break;
            }
        }

        sc = new Scanner(in);
    }

    /**
     * output type
     * @return output type
     */
    public OutputTypeEnum getOtype() {
        return otype;
    }

    /**
     * checks if encrypt or decrypt selected
     * @return true for encryption false for decryption
     */
    public boolean isEncrypt() {
        return encrypt;
    }

    /**
     * normal running required
     * @return false to run assignment
     */
    public boolean isNormal() {
        return normal;
    }

    /**
     * fips197 style output and normal running of encryption
     * @return true if fips style required
     */
    public boolean isFips() {
        return fips;
    }

    /**
     * the output stream
     * @return current output stream
     */
    public PrintStream out() {
        return out;
    }

    /**
     * closes output and input streams
     */
    public void close(){
        if(out != System.out){
            out.flush();
            out.close();
        }
        if(in != System.in){
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * returns input scanner
     * @return input scanner
     */
    public Scanner scan() {
        return sc;
    }
}
