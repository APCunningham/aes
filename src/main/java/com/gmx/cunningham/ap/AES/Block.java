package com.gmx.cunningham.ap.AES;

import javax.xml.bind.DatatypeConverter;

/**
 * Holds 128-bit block of text
 * @author Andrew Cunningham
 * @author Jake Spencer-Goodsir
 * @since 02/05/2015
 */
public class Block {
    private int[][] block;

    /**
     * Constructor
     * creates a new zero initialised 4x4 byte block
     */
    public Block(){
        block = new int[4][4];
    }

    /**
     * Constructor
     * Creates a new 4x4 byte block with provided integer array
     * @param block integer matrix array
     * @throws RuntimeException if not a 4x4 multidimensional array
     */
    public Block(int[][] block){
        if(block.length!=4||block[0].length!=4||
                block[1].length!=4||block[2].length!=4||
                block[3].length!=4) throw new RuntimeException("4x4 blocks only");
        this.block = block;
    }

    /**
     * Constructor
     * Creates a new 4x4 byte block using array provided put in column first form
     * @param arr array of integers
     * @throws RuntimeException if array not length 16
     */
    public Block(int[] arr){
        this();
        if(arr.length!=16) throw new RuntimeException("Block too small");
        for(int i=0;i<4;i++){
            block[0][i] = arr[i*4];
            block[1][i] = arr[i*4+1];
            block[2][i] = arr[i*4+2];
            block[3][i] = arr[i*4+3];
        }
    }

    /**
     * Constructor
     * Creates a new 4x4 byte block from provided string.
     * String must be one of the following:
     * Any String of length 16
     * hexadecimal string of length 32
     * binary string of length 128
     * @param s input string
     * @throws RuntimeException if string not length 16, 32 or 128
     */
    public Block(String s){
        this();
        //checks string length and throws unchecked exception
        if(s.length()!=16&&s.length()!=32&&s.length()!=128) throw new RuntimeException("Block too small or too large");

        //selects input type based on string length
        switch(s.length()) {
            case 16:
                //any string
                for (int i = 0; i < 4; i++){
                    block[0][i] = (int) s.charAt(i*4);
                    block[1][i] = (int) s.charAt(i*4+1);
                    block[2][i] = (int) s.charAt(i*4+2);
                    block[3][i] = (int) s.charAt(i*4+3);
                }
                break;
            case 32:
                //hexadecimal string
                for (int i = 0; i < 4; i++){
                    block[0][i] = Integer.valueOf(s.substring(i*8, i*8+2), 16);
                    block[1][i] = Integer.valueOf(s.substring(i*8+2, i*8+4), 16);
                    block[2][i] = Integer.valueOf(s.substring(i*8+4, i*8+6), 16);
                    block[3][i] = Integer.valueOf(s.substring(i*8+6, i*8+8), 16);
                }
                break;
            case 128:
                //binary string
                for(int i=0;i<4;i++){
                    block[0][i] = Integer.valueOf(s.substring(i*32, i*32+8), 2);
                    block[1][i] = Integer.valueOf(s.substring(i*32+8, i*32+16), 2);
                    block[2][i] = Integer.valueOf(s.substring(i*32+16, i*32+24), 2);
                    block[3][i] = Integer.valueOf(s.substring(i*32+24, i*32+32), 2);
                }
                break;
        }
    }

    /**
     * Returns current value in position provided in the column matrix
     * @param row row of desired value
     * @param col col of desired value
     * @return value in position
     */
    public int get(int row, int col){
        return block[row][col];
    }

    /**
     * Sets position in column matrix
     * @param row of cell
     * @param col of cell
     * @param i new value
     */
    public void set(int row, int col, int i){
        block[row][col] = i;
    }

    /**
     * Returns a col from column matrix
     * @param col index of column
     * @return column from column matrix
     */
    public int[] getColumn(int col){
        return new int[]{
                block[0][col],
                block[1][col],
                block[2][col],
                block[3][col]
            };

    }

    /**
     * Returns the entire matrix
     * @return entire matrix
     */
    public int[][] matrix(){
        return block;
    }

    /**
     * Sets a row
     * @param row index of row
     * @param x0 new value for col[0]
     * @param x1 new value for col[1]
     * @param x2 new value for col[2]
     * @param x3 new value for col[3]
     */
    public void setRow(int row, int x0, int x1, int x2, int x3){
        block[row] = new int[]{x0,x1,x2,x3};
    }

    /**
     * Sets a column
     * @param col index of column
     * @param y0 new value for row[0]
     * @param y1 new value for row[1]
     * @param y2 new value for row[2]
     * @param y3 new value for row[3]
     */
    public void setColumn(int col, int y0, int y1, int y2, int y3){
        block[0][col] = y0;
        block[1][col] = y1;
        block[2][col] = y2;
        block[3][col] = y3;
    }

    /**
     * Default is hexadecimal
     * @return hexadecimal string of block
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                sb.append(String.format("%02x",block[j][i]));
            }
        }
        return sb.toString();
    }

    /**
     * Returns a base64 string of block
     * @return base64 string of block
     */
    public String toBase64(){
        byte[] buff = new byte[16];
        for(int i=0;i<4;i++){
            buff[i*4] = (byte)block[i][0];
            buff[i*4+1] = (byte)block[i][1];
            buff[i*4+2] = (byte)block[i][2];
            buff[i*4+3] = (byte)block[i][3];
        }
        return DatatypeConverter.printBase64Binary(buff);
    }

    /**
     * Returns binary string of this block
     * @return binary string of this block
     */
    public String toBinaryString(){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                String temp = Integer.toBinaryString(block[j][i]);
                sb.append(zeros(8-temp.length())).append(temp);
            }
        }
        return sb.toString();
    }

    /**
     * Returns a hexadecimal string of this block
     * @return hexadecimal string of this block
     */
    public String toHexString(){
        return toString();
    }

    /**
     * Returns a matrix style string of this block
     * @return matrix style string of this block
     */
    public String toMatrixString(){
        String out = "";
        for(int i=0;i<block.length;i++) {
            out += String.format("%02x | %02x | %02x | %02x\n",
                    block[i][0], block[i][1], block[i][2], block[i][3]);
            out += i!=3?"-----------------\n":"";
        }
        return out;
    }

    /**
     * Returns string based on output type required
     * @param ot output type required
     * @return string of output type
     */
    public String to(OutputTypeEnum ot){
        switch(ot){
            case hex: return toHexString();
            case b64: return toBase64();
            case bin: return toBinaryString();
            case mat: return toMatrixString();
        }
        return toString();
    }

    //pads binary strings with zeros
    private String zeros(int z){
        StringBuilder s = new StringBuilder();
        for(int i=0;i<z;i++){
            s.append("0");
        }
        return s.toString();
    }

    /**
     * Compares two blocks and returns the number of different bits
     * @param b1 first block
     * @param b2 second block
     * @return number of different bits
     */
    public static int difference(Block b1, Block b2){
        int diff = 0;
        String bs1 = b1.toBinaryString();
        String bs2 = b2.toBinaryString();
        for(int i=0;i<bs1.length();i++) diff += bs1.charAt(i)!=bs2.charAt(i)?1:0;
        return diff;
    }
}
